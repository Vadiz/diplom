<?php
session_start();
if (!$_SESSION['sotrudnik']) {
    header('Location: /registr/admin.php');
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

    <!-- Профиль -->

    <form>
        
        <h2 style="margin: 10px 0;"><?= $_SESSION['sotrudnik']['fio'] ?></h2>
        <a href="#"><?= $_SESSION['sotrudnik']['login'] ?></a>
        <a href="vendor/logout.php" class="logout">Выход</a>
    </form>

</body>
</html>