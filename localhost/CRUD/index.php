<?php

/*
 * Подключаем файл для получения соединения к базе данных (PhpMyAdmin, MySQL)
 */

require_once 'config/connect.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Заявки</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <main>
        <div class="klient-out"></div>
        <div class="container">

        <div class="row">
            <div class="col-25">
            <label for="glicevoi">Лицеой счёт</label>
            </div>
            <div class="col-75">
            <input type="text" id="glicevoi" placeholder="Введите номер лицевого счёта...">
            </div>
            </div>

            <div class="row">
            <div class="col-25">
            <label for="gfio">Фамилия имя отчество</label>
            </div>
            <div class="col-75">
            <input type="text" id="gfio" placeholder="Введите ФИО клиента...">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
            <label for="gtelefon">Телефон</label>
            </div>
            <div class="col-75">
            <input type="text" id="gtelefon" placeholder="Введите нормер телефона...">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
            <label for="gadres">Адрес</label>
            </div>
            <div class="col-75">
            <input type="text" id="gadres" placeholder="Введите адрес клиента...">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
            <label for="gusluga">Подключенные услуги</label>
            </div>
            <div class="col-75">
            <textarea id="gusluga" placeholder="Введите подключенные услуги..."></textarea>
            </div>
        </div>
        <input type="hidden" id="gid">
        
        <button class="add-to-db">Обновить</button>
    </main>
<script src="jquery-3.2.1.min.js"></script>
<script src="admin.js"></script>
</body>

<style>
    th, td {
        padding: 10px;
    }

    th {
        background: #606060;
        color: #fff;
    }

    td {
        background: #b5b5b5;
    }
</style>
<body>
    <table>
        <tr>
            <th>id</th>
            <th>klient_id</th>
            <th>name</th>
            <th>Description</th>    
        </tr>

        <?php

            /*
             * Делаем выборку всех строк из таблицы "products"
             */

            $orders = mysqli_query($connect, "SELECT * FROM `orders`");

            /*
             * Преобразовываем полученные данные в нормальный массив
             */

            $orders = mysqli_fetch_all($orders);

            /*
             * Перебираем массив и рендерим HTML с данными из массива
             * Ключ 0 - id
             * Ключ 1 - klient_id
             * Ключ 2 - name
             * Ключ 3 - description
             */

            foreach ($orders as $order) {
                ?>
                    <tr>
                        <td><?= $order[0] ?></td>
                        <td><?= $order[1] ?></td>
                        <td><?= $order[2] ?></td>
                        <td><?= $order[3] ?></td>
                        <td><a href="product.php?id=<?= $order[0] ?>">View</a></td>
                        <td><a href="update.php?id=<?= $order[0] ?>">Update</a></td>
                        <td><a style="color: red;" href="vendor/delete.php?id=<?= $order[0] ?>">Delete</a></td>
                    </tr>
                <?php
            }
        ?>
    </table>
    <h3>Создать заказ</h3>
    <body>
        <form action="vendor/create.php" method="post">
        <div class="container">

        <div class="row">
            <div class="col-25">
            <label>Лицеой счёт</label>
            </div>
            <div class="col-75">
            <input type="text" name="klient_id" placeholder="Введите номер лицевого счёта...">
            </div>
            </div>

            <div class="row">
            <div class="col-25">
            <label>Номер и название услуги</label>
            </div>
            <div class="col-75">
            <input type="text" name="name" placeholder="Введите номер и название заказа...">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
            <label>Описание</label>
            </div>
            <div class="col-75">
            <textarea type="text" name="description" placeholder="Введите описание заказа..."></textarea>
            </div>
        </div>

        <div class="row">
        <input type="submit" value="Добаить заказ">
        </div>
    </form>
</body>
</html>
