<?php

    /*
     * Подключаем файл для получения соединения к базе данных (PhpMyAdmin, MySQL)
     */

    require_once 'config/connect.php';

    /*
     * Получаем ID продукта из адресной строки - /product.php?id=1
     */

    $order_id = $_GET['id'];

    /*
     * Делаем выборку строки с полученным ID выше
     */

    $order = mysqli_query($connect, "SELECT * FROM `orders` WHERE `id` = '$order_id'");

    /*
     * Преобразовывем полученные данные в нормальный массив
     * Используя функцию mysqli_fetch_assoc массив будет иметь ключи равные названиям столбцов в таблице
     */

    $order = mysqli_fetch_assoc($order);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Редактирование заказа</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <h3>Pедактирование заказа</h3>
    <form action="vendor/update.php" method="post">
        <input type="hidden" name="id" value="<?= $order['id'] ?>">

        <div class="container">
        <div class="row">
            <div class="col-25">
            <label for="name">Название заказа</label>
            </div>
            <div class="col-75">
            <input type="text" name="name" value="<?= $order['name'] ?>">
            </div>
            </div>

            <div class="row">
            <div class="col-25">
            <label for="description">Описание заказа</label>
            </div>
            <div class="col-75">
            <textarea name="description"><?= $order['description'] ?></textarea>
            </div>
            </div>

                <br> <br>
        <div class="row">
        <input type="submit" value="Изменить данные">
        </div>
    </form>
</body>
</html>