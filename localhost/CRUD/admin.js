function init() {
    $.post(
        "core.php",
        {
            "action" : "init"
        },
        showKlient
    );
}

function showKlient(data) {
    data = JSON.parse(data);
    console.log(data);
    var out='<select>';
    out +='<option data-id="0">Новый клиент</option>';
    for (var id in data) {
        out +=`<option data-id="${id}">${data[id].licevoi}</option>`;
    }
    out +='</select>';
    $('.klient-out').html(out);
    $('.klient-out select').on ('change', selectKlient);
}

function selectKlient(){
    var id =$('.klient-out select option:selected').attr('data-id');
    console.log(id);
    $.post(
        "core.php",
        {
            "action" : "selectOneKlient",
            "gid": id
        },
        function(data){
            data = JSON.parse(data);
            $('#glicevoi').val(data.licevoi);
            $('#gfio').val(data.fio);
            $('#gtelefon').val(data.telefon);
            $('#gadres').val(data.adres);
            $('#gusluga').val(data.usluga);
	        $('#gid').val(data.id);
        }
    );
 }

function saveToDb() {
    var id = $('#gid').val();
    if (id!=''){
        $.post(
            "core.php",
            {
                "action" : "updateKlient",
                "id" : id,
                "glicevoi" : $('#glicevoi').val(),
                "gfio" : $('#gfio').val(),
                "gtelefon" : $('#gtelefon').val(),
                "gadres" : $('#gadres').val(),
                "gusluga" : $('#gusluga').val()
            },
            function(data){
                if (data==1) {
                    alert('Данные клиента обновлены')
                    init();
                }
                else {
                console.log(data);
                }
            }
        );
    }
    else {
        console.log('new');
        $.post(
            "core.php",
            {
                "action" : "newKlient",
                "id" : 0,
                "glicevoi" : $('#glicevoi').val(),
                "gfio" : $('#gfio').val(),
                "gtelefon" : $('#gtelefon').val(),
                "gadres" : $('#gadres').val(),
                "gusluga" : $('#gusluga').val()
            },
            function(data){
                if (data==1) {
                    alert('Клиент добавлен')
                    init();
                }
                else {
                console.log(data);
                }
            }
        );
    }
}

$(document).ready(function () {
   init();
   $('.add-to-db').on('click',saveToDb);
});