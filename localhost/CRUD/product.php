<?php

    /*
     * Подключаем файл для получения соединения к базе данных (PhpMyAdmin, MySQL)
     */

    require_once 'config/connect.php';

    /*
     * Получаем ID продукта из адресной строки - /product.php?id=1
     */

    $order_id = $_GET['id'];

    /*
     * Делаем выборку строки с полученным ID выше
     */

    $order = mysqli_query($connect, "SELECT * FROM `orders` WHERE `id` = '$order_id'");

    /*
     * Преобразовывем полученные данные в нормальный массив
     * Используя функцию mysqli_fetch_assoc массив будет иметь ключи равные названиям столбцов в таблице
     */

    $order = mysqli_fetch_assoc($order);

    /*
     * Делаем выборку всех строк комментариев с полученным ID продукта выше
     */

    $orders = mysqli_query($connect, "SELECT * FROM `orders` WHERE `klient_id` = '$klient_id'");

    /*
     * Преобразовывем полученные данные в нормальный массив
     */

    $orders = mysqli_fetch_all($orders);
?>

<!doctype html>
<html lang="en">
<head>
    <title>order</title>
</head>
<body>
    <h2>Title: <?= $order['klient_id'] ?></h2>
    <h4>Price: <?= $order['name'] ?></h4>
    <p>Description: <?= $order['description'] ?></p>

    <hr>

    <h3>Add comment</h3>
    <form action="vendor/create_comment.php" method="post">
        <input type="hidden" name="id" value="<?= $order['id'] ?>">
        <textarea name="body"></textarea> <br><br>
        <button type="submit">Add comment</button>
    </form>

    <hr>

    <h3>Comments</h3>
    <ul>
        <?php

            /*
             * Перебираем массив с комментариями и выводим
             */

            foreach ($orders as $orders) {
            ?>
                <li><?= $order[2] ?></li>
            <?php
            }
        ?>
    </ul>
</body>
</html>