<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "diplom";

function connect(){
    $conn = mysqli_connect("localhost", "root", "", "diplom");
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    mysqli_set_charset($conn, "utf8");
    return $conn;
}

function init(){
    //вывожу список клиентов
    $conn = connect();
    $sql = "SELECT * FROM klient";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $out = array();
        while($row = mysqli_fetch_assoc($result)) {
            $out[$row["id"]] = $row;
        }
        echo json_encode($out);
    } else {
        echo "0";
    }
    mysqli_close($conn);
}

function selectOneKlient(){
    $conn = connect();
    $id = $_POST['gid'];
    $sql = "SELECT * FROM klient WHERE id = '$id'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        echo json_encode($row);
    } else {
        echo "0";
    }
    mysqli_close($conn);
}

function updateKlient(){
    $conn = connect();
    $id = $_POST['id'];
    $licevoi = $_POST['glicevoi'];
    $fio = $_POST['gfio'];
    $telefon = $_POST['gtelefon'];
    $adres = $_POST['gadres'];
    $usluga = $_POST['gusluga'];

    $sql = "UPDATE klient SET licevoi = '$licevoi',  fio = '$fio', telefon = '$telefon', adres = '$adres', usluga = '$usluga' WHERE id = '$id'";

    if (mysqli_query($conn, $sql)) {
        echo "1";
    } else {
    echo "Error updating record: " . mysqli_error($conn);
}

    mysqli_close($conn);
 
}

function newKlient(){
    $conn = connect();
    $licevoi = $_POST['glicevoi'];
    $fio = $_POST['gfio'];
    $telefon = $_POST['gtelefon'];
    $adres = $_POST['gadres'];
    $usluga = $_POST['gusluga'];

    $sql = "INSERT INTO klient (licevoi, fio, telefon, adres, usluga)
    VALUES ('$licevoi', '$fio', '$telefon', '$adres', '$usluga')";
    
    if (mysqli_query($conn, $sql)) {
      echo "1";
    } else {
      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);

}

function loadKlient(){
    $conn = connect();
    $sql = "SELECT * FROM klient";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $out = array();
    while($row = mysqli_fetch_assoc($result)) {
        $out[$row["id"]] = $row;
    }
    echo json_encode($out);
} else {
    echo "0";
}
mysqli_close($conn);
}