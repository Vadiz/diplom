-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 08 2021 г., 16:07
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `diplom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `klient`
--

CREATE TABLE `klient` (
  `id` int(4) NOT NULL,
  `licevoi` int(4) DEFAULT NULL,
  `fio` varchar(50) NOT NULL,
  `telefon` bigint(11) NOT NULL,
  `adres` varchar(200) NOT NULL,
  `usluga` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `klient`
--

INSERT INTO `klient` (`id`, `licevoi`, `fio`, `telefon`, `adres`, `usluga`) VALUES
(1, 1234, 'Иванов Степан Вадимович', 812345671010, 'г. Липецк, ул.Мовсовская, д 3, кв 35', 'Пакет спутникового телевиденья \"Зпритель\" (350 руб/мес)\nПроводной интернет \"200 гб\" (100 рублей/мес)\n'),
(17, 1235, 'Сидоров Пётр Александроич', 89053746284, 'г. Липецк, ул.Катукова, д 4, кв 253', 'Проводной интернет (350 руб/мес)');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(4) NOT NULL,
  `klient_id` int(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `klient_id`, `name`, `description`) VALUES
(39, 1234, '1. Замена кабеля (25 метров)', 'Степан Вадимович, 8904756452, г. Липецк, ул.Мовсовская, д 3, кв 35'),
(42, 1234, 'z', 'z'),
(43, 1235, 'Проверка роутера', 'Пётр Александроич г. Липецк, ул.Катукова, д 4, кв 253');

-- --------------------------------------------------------

--
-- Структура таблицы `sotrudnik`
--

CREATE TABLE `sotrudnik` (
  `id` int(11) NOT NULL,
  `fio` varchar(50) NOT NULL,
  `login` varchar(11) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sotrudnik`
--

INSERT INTO `sotrudnik` (`id`, `fio`, `login`, `password`) VALUES
(1, 'qwerty', 'qwerty', 'qwerty');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `licevoi` (`licevoi`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `klient_id` (`klient_id`);

--
-- Индексы таблицы `sotrudnik`
--
ALTER TABLE `sotrudnik`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `klient`
--
ALTER TABLE `klient`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
